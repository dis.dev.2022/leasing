/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
import {Fancybox} from "@fancyapps/ui";

// Инициализация слайдеров
function initSliders() {

    // Перечень слайдеров

    if (document.querySelector('[data-comments-slider]')) {
        new Swiper('[data-comments-slider]', {
            modules: [Autoplay, Pagination],
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 18,
            autoHeight: true,
            speed: 800,
            pagination: {
                el: '[data-comments-pagination]',
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 18,
                },
                1200: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                },
                1408: {
                    slidesPerView: 3,
                    spaceBetween: 24,
                },
            },
            on: {

            }
        });
    }

    // Why slider
    const whySelector = document.querySelector('[data-why-slider]');
    if (whySelector) {
        let whyQuerySize  = 768;
        let whySlider = null;

        function whySliderInit() {
            if(!whySlider) {
                whySlider = new Swiper('[data-why-slider]', {
                    init: true,
                    loop: false,
                    modules: [Pagination],
                    slidesPerView: 'auto',
                    spaceBetween: 0,
                    pagination: {
                        el: '[data-why-pagination]',
                        clickable: true,
                    },
                });
            }
        }

        function whySliderDestroy() {
            if(whySlider) {
                whySlider.destroy();
                whySlider = null;
            }
        }

        if (document.documentElement.clientWidth > whyQuerySize) {
            whySliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < whyQuerySize) {
                whySliderInit()
            }
            else {
                whySliderDestroy()
            }
        });
    }

    document.addEventListener('click', stories);

    function stories(event) {
        if (event.target.closest('[data-story]')) {
            let startSlide = event.target.closest('[data-story]').dataset.story;

            let storySlider = new Swiper('[data-stories]', {
                init: false,
                loop: false,
                initialSlide: startSlide,
                modules: [Autoplay, Pagination],
                slidesPerView: 'auto',
                spaceBetween: 48,
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
            });

            new Fancybox(
                [
                    {
                        closeButton: false,
                        dragToClose: false,
                        src: "#stories",
                        type: "inline",
                    },
                ],
                {
                    on: {
                        done: (fancybox, slide) => {
                            storySlider.init();
                        },
                        destroy: (fancybox, slide) => {
                            console.log('destroy');
                            storySlider.destroy();
                        },
                    },
                }
            );
        }
    }

    // Product gallery
    if (document.querySelector('[data-gallery]')) {
        const gallery = new Swiper('[data-gallery]', {
            init: true,
            loop: false,
            slidesPerView: 1,
            spaceBetween: 30,
        });

        document.addEventListener('click', function (event){
            if (event.target.closest('[data-gallery-slide]')) {
                let nextSlide = event.target.closest('[data-gallery-slide]').dataset.gallerySlide;
                gallery.slideTo(nextSlide, 600);
            }
        });
    }

    // Showcase slider
    const showcaseSelector = document.querySelector('[data-showcase]');
    if (showcaseSelector) {
        let mediaQuerySize  = 1199;
        let showcaseSlider = null;

        function showcaseSliderInit() {
            if(!showcaseSlider) {
                showcaseSlider = new Swiper('[data-showcase]', {
                    init: true,
                    loop: false,
                    modules: [Navigation],
                    slidesPerView: 3,
                    spaceBetween: 0,
                });
            }
        }

        function showcaseSliderDestroy() {
            if(showcaseSlider) {
                showcaseSlider.destroy();
                showcaseSlider = null;
            }
        }

        if (document.documentElement.clientWidth > mediaQuerySize) {
            showcaseSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth > mediaQuerySize) {
                showcaseSliderInit()
            }
            else {
                showcaseSliderDestroy()
            }

        });
    }

}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
