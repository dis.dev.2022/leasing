export default () => {
    const pageNavToggle = document.querySelectorAll('[data-nav-toggle]');

    if (pageNavToggle) {
        pageNavToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = document.querySelector('body');
                self.classList.toggle('nav-open');
                return false;
            });
        });
    }
};
