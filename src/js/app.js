"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import tabs from './components/tabs.js'; // Tabs
import maskPhone from './forms/phone-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Choices from 'choices.js'; // Select plugin
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import productCheckout from "./components/product.js";
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();


// Маска для ввода номера телефона
maskPhone('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Вкладки (tabs)
tabs();

// Custom Select
document.querySelectorAll('[data-select]').forEach(el => {
    const prettySelect = new Choices(el,{
        allowHTML: true,
        searchEnabled: false
    });
});

document.querySelectorAll('[data-select-sm]').forEach(el => {
    const prettySelectSm = new Choices(el,{
        allowHTML: true,
        searchEnabled: false,
        removeItemButton: true,
        classNames: {
            containerOuter: 'choices choices--sm',
            containerInner: 'choices__inner choices__inner--sm',
        }
    });
});

document.querySelectorAll('[data-select-border]').forEach(el => {
    const prettySelectBorder = new Choices(el,{
        allowHTML: true,
        searchEnabled: false,
        removeItemButton: true,
        classNames: {
            containerOuter: 'choices choices--border',
            containerInner: 'choices__inner choices__inner--border ',
        }
    });
});

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false,
    closeButton: false
});

// Sliders
import "./components/sliders.js";
import Swiper, {Pagination} from "swiper";

// Spoilers
Spoilers();

// Dropdown
Dropdown();


const filterToggle = document.querySelectorAll('[data-filter-toggle]');

if (filterToggle) {
    filterToggle.forEach(el => {
        el.addEventListener('click', (e) => {
            const self = document.querySelector('body');
            self.classList.toggle('filter-open');
            return false;
        });
    });
}

// Sticky block
(function(){
    if(document.querySelector('[data-sticky-block]')) {
        let a = document.querySelector('[data-sticky-block]'), b = null, P = 20;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
            if (b == null) {
                let Sa = getComputedStyle(a, ''), s = '';
                for (let i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');
                b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                a.insertBefore(b, a.firstChild);
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px';
                a.style.padding = '0';
                a.style.border = '0';
            }
            let Ra = a.getBoundingClientRect(),
                R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('[data-sticky-end]').getBoundingClientRect().top + 0);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
            if ((Ra.top - P) <= 0) {
                if ((Ra.top - P) <= R) {
                    b.className = 'stop';
                    b.style.top = - R +'px';
                } else {
                    b.className = 'sticky';
                    b.style.top = P + 'px';
                }
            } else {
                b.className = '';
                b.style.top = '';
            }
            window.addEventListener('resize', function() {
                a.children[0].style.width = getComputedStyle(a, '').width
            }, false);
        }
    }
})()


const anchors = document.querySelectorAll('[data-anchor]');

for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault()

        const blockID = anchor.getAttribute('href').substr(1)

        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}

/* Настройка цветов */
import "./components/settings.js";

// Product Checkout
productCheckout();
